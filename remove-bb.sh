#!/bin/sh

if ! [ $(id -u) = 0 ]; then
   echo "Please run as root or with sudo..."
   exit 1
fi

if [ -d "/bake-buddy/signer" ]; then
  echo "Removing /bake-buddy/signer..."
  ami --path=/bake-buddy/signer stop || echo "Failed to stop signer services. Please reboot machine after this script finishes..."; 
  ami --path=/bake-buddy/signer remove --all && \
  rm -r /bake-buddy/signer && \
  echo "/bake-buddy/signer removed" || echo "Failed to remove /bake-buddy/signer"
fi

if [ -d "/bake-buddy/node" ]; then
  echo "Removing /bake-buddy/node..."
  ami --path=/bake-buddy/node stop || echo "Failed to stop node services. Please reboot machine after this script finishes..."; 
  ami --path=/bake-buddy/node remove --all && \
  rm -r /bake-buddy/node && 
  echo "/bake-buddy/node removed" || echo "Failed to remove /bake-buddy/node"
fi

echo "Removal complete!"