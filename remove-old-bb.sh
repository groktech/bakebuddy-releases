#!/bin/sh

if ! [ $(id -u) = 0 ]; then
   echo "Please run as root or with sudo..."
   exit 1
fi

if [ -d "/bake-buddy/client" ]; then
  echo "Removing /bake-buddy/client..."
  ami --path=/bake-buddy/client stop || echo "Failed to stop client services. Please reboot machine after this script finishes..."; 
  ami --path=/bake-buddy/client remove --all && \
  rm -r /bake-buddy/client && \
  echo "/bake-buddy/client removed" || echo "Failed to remove /bake-buddy/client"
fi

if [ -d "/bake-buddy/server" ]; then
  echo "Removing /bake-buddy/server..."
  ami --path=/bake-buddy/server stop || echo "Failed to stop server services. Please reboot machine after this script finishes..."; 
  ami --path=/bake-buddy/server remove --all && \
  rm -r /bake-buddy/server && 
  echo "/bake-buddy/server removed" || echo "Failed to remove /bake-buddy/server"
fi

echo "Removal complete!"