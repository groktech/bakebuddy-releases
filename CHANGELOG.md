0.0.17
- changed folder structure
- various fixes
- open source

0.0.13
- split baking and endorsing to separate sections
- added column headers to baking and endorsing sections

0.0.12
- changed baking/endorsing rights default feed to tztk
- changed baking/endorsing rights to collapsible

0.0.11
- added Reports section
- logs in flow is reversed - new are on the top
- fixed donation address hit box

0.0.10
- unified styling
fixed:
- setup reports success even when bootstrap fails
- wrong date format in logs created by BB
